/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * Write your transction processor functions here
 */

/**
 * Sample transaction
 * @param {org.cannx.Payment} sampleTransaction
 * @transaction
 */
 async function paymentTransaction(payment) {

// http://localhost:3000/api/payment/5d77c9f55a186e65d7693b9a GET
//    {
//     "Sellers": [
//         "Setto",
//         "Cannx"
//     ],
//     "Products": [
//         {
//             "Product Name": "Omega369-CBD",
//             "Product Price": 27.45
//         },
//         {
//             "Product Name": "Shake-CBD",
//             "Product Price": 17.99
//         }
//     ],
//     "Total": 45.44,
//     "PaymentType": "Cryptocurrency",
//     "Buyer": "kavallare@gmail.com"
// }

// o String paymentId
// o String[] sellers
// o String[] products
// o Double total
// o String paymentType
// o String buyerEmail
// --> User buyer

     // payment.commodity.owner = payment.newOwner;
     // let assetRegistry = await getAssetRegistry('org.cannx.Commodity');
     // await assetRegistry.update(payment.commodity);

     // Get the serializer.
     const serializer = getSerializer();
     // Serialize a vehicle.
     const rjsfy = serializer.toJSON(payment);

     // 192.168.1.140
     const URL_MONGOOSE_GET = 'http://192.168.1.140:3033/api/payment/' + rjsfy.paymentId;

     const options = {
        method: 'GET',
        uri: URL_MONGOOSE_GET,
        headers: {
            'content-type': 'application/json'
        },
        json: true
      };
     const result = await request.get(options);
     console.log(' result ::; ', result );

 }


 // async function tradeCommodity(resource) {
 //     // Get the serializer.
 //     const serializer = getSerializer();
 //     // Serialize a vehicle.
 //     const rjsfy = serializer.toJSON(resource);
 //     console.log('assetCommodity {} ::; ', {rjsfy} );
 //     console.log('assetCommodity ::; ', rjsfy );
 //     // console.log('assetCommodity ::; ', rjsfy.photo );
 //     // console.log('assetCommodity ::; ', rjsfy.commodity );
 //     // console.log('assetCommodity ::; ', rjsfy.newOwner );
 //     // console.log('assetCommodity ::; ', rjsfy.transactionId );
 //     // console.log('assetCommodity ::; ', rjsfy.timestamp );
 //
 //     const dataMongoose = {
 //       "class": rjsfy.$class,
 //       "photo": rjsfy.photo,
 //       "paymentId": rjsfy.paymentId,
 //       "paymentType": rjsfy.paymentType,
 //       "amount": rjsfy.amount,
 //       "commodity": rjsfy.commodity,
 //       "newOwner": rjsfy.newOwner,
 //       "transactionId": rjsfy.transactionId,
 //       "timestamp": rjsfy.timestamp
 //     }
 //     // const URL_MONGOOSE_POST = 'http://172.31.19.127:3033/api/transact';
 //
 //     const URL_MONGOOSE_POST = 'http://192.168.0.36:3033/api/transact';
 //     // const result = await request.post({ uri: URL_MONGOOSE_POST, json: dataMongoose });
 //
 //     const options = {
 //        method: 'POST',
 //        uri: URL_MONGOOSE_POST,
 //        headers: {
 //            'content-type': 'application/json'
 //        },
 //        json: true
 //      };
 //     const result = await request.get(options);
 //     console.log( 'CHAINCODE assetCommodity result::; ', result );
 //
 //     // resource.commodity.owner = resource.newOwner;
 //     // let assetRegistry = await getResourceRegistry('org.example.son.Commodity');
 //     // await assetRegistry.update(resource.commodity);
 // }

//  function handlePost(postTransaction) {
//     var url = 'https://composer-node-red.mybluemix.net/compute';
//
//     return post( url, postTransaction)
//       .then(function (result) {
//         // alert(JSON.stringify(result));
//           postTransaction.asset.value = 'Count is ' + result.body.sum;
//           return getAssetRegistry('org.example.sample.SampleAsset')
//           .then(function (assetRegistry) {
//               return assetRegistry.update(postTransaction.asset);
//           });
//       });
// }
