const mongoose = require('mongoose');

const uri = 'mongodb://localhost:27017/cannx-mrktplace'

const options = {
  useFindAndModify:false,
  useNewUrlParser: true,
  useCreateIndex:true,
  auto_reconnect: true,
  poolSize: 15,
  keepAlive: true,
  keepAliveInitialDelay: 270000
}

mongoose.connect(uri, options)
.then(() => {
  console.log(' Mongodb Connected :D ');

  mongoose.connection.on('error', (err) => {
    console.log('mongoose connection ::;' , err);
  });
  mongoose.connection.on('reconnected', () => {
    console.log('Reconnected to MongoDB');
  });
})
.catch((err) => {
  console.log('rejected promise ::; ', err);
  mongoose.disconnect();
});


module.exports = mongoose;
