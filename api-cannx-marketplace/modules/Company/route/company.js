
const express = require('express');
const router = express.Router();

const Company = require('../controller/company')

// router post route company, validating company, create thruout company
router.post('/company', Company.create);

router.get('/company-product/:_id', Company.informationCompanyProduct);

module.exports = router
