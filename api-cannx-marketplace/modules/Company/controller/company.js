
const Company = require('../model/company');
const Product = require('../../Product/model/product');

const create = ( req, res ) => {

	try {

		
		// {
			//   "firstName": "Augusto",
			//   "lastName": "Fonseca",
			//   "username": "Aufo",
			//   "companyName": "Setto",
			//   "email": "aufo@setto.cc",
			//   "market": ["Grower", "Supplements"]
			// }
		const objCompany = {
			firstName: req.body.firstName || 'Erni',
			lastName: req.body.lastName || 'Souza',
      username: req.body.username || 'ernisouza',
      companyName: req.body.companyName || 'Cannx',
      email: req.body.email || 'erni@cannx.com',
			market: req.body.market || ['Grower', 'Supplements'],
		}
		// console.log('req ::; ', objCompany.email);
		// console.log( 'objCompany ::; ', objCompany);

		// creates company info in database
    // const company = await Company.create(objCompany);
    const company = Company.create(objCompany);
    company.then( (newCompany) => {
      // console.log('newCompany ::; ', newCompany);
      res.status(201).json(newCompany);
    })
    .catch( err => res.status(400).json(err))
		// console.log( 'company ::; ', company);


	} catch (error) {
		res.status(400).json(error);
	}
}

const informationCompanyProduct = (req, res) => {

  try {

    const companyId = req.params._id

    Company.
    findById(companyId).
    populate('products'). // only works if we pushed refs to children
    exec((err, data) => {
      if (err) return err;
      console.log(data);
      const companiesData = data

      res.status(200).json(companiesData)
      // res.status(200).json(reduceCompany)
    });

  } catch (error) {
    // console.log(error);
    res.status(400).json(error);
  }
}

const objModulesToExport = {
	create,
	informationCompanyProduct
}

module.exports = objModulesToExport
