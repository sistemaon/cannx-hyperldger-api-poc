
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

// mongoose company schema mongo
const CompanySchema = new Schema({
	firstName: {type: String, required: true},
	lastName: {type: String, required: true},
  username: {type: String, required: true},
	companyName: {type: String, required: true},
  email: {type: String, required: true, unique: false},
	market: {type: Array, required: true},

  products: [ { type: Schema.Types.ObjectId, ref: 'Product' } ],

});

// mongoose company model mongo
const Company = mongoose.model('Company', CompanySchema);

module.exports = Company;
