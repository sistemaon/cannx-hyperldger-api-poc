
const express = require('express');
const router = express.Router();

const Product = require('../controller/product');

// router post route product register, validating product register, product create thruout register
router.post('/product/:_id', Product.createProduct);

router.get('/product-company/:_id', Product.informationProductCompany);

// router.post('/indicate/:_id', Product.indicateStudent);

// router.get('/information/:_id', Product.informationTeacherStudent);
// router.post('/indicate', Product.indicateStudent);

module.exports = router
