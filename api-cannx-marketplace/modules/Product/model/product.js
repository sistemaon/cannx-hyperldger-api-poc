
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

// mongoose product schema mongo
const ProductSchema = new Schema({
	productName: {type: String, required: true},
	amount: {type: Number, required: true},
	price: {type: Number, required: true},

	company: { type: Schema.Types.ObjectId, ref: 'Company' },

});

// mongoose product model mongo
const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;
