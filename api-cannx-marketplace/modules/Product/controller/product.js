
const Product = require('../model/product');
const Company = require('../../Company/model/company');

const createProduct = (req, res) => {

  try {
    // companyId
    const companyId = req.params._id;
    console.log(' companyId ::; ', companyId);

    Company.findById(companyId, (err, company) => {

      const objProduct = {
        productName: req.body.productName || 'Shake-CBD',
        amount: req.body.amount || 12,
        price: req.body.price || 17.99,
        company: companyId
      }

      const product = Product.create(objProduct);
      product.then((newProduct) => {

          // console.log('newProduct ::; ', newProduct);

          const companyProductsArr = company.products
          const companyProductsAddArr = companyProductsArr.push(newProduct._id)
          const query = {
            _id: company._id
          }
          const mod = {
            products: companyProductsArr
          }
          const options = {
            runValidators: true
          }

          Company.updateOne(query, mod, options, (err, data) => {
            res.status(201).json(newProduct);
          });

        })
        .catch(err => res.status(400).json(err))
      // console.log( 'product ::; ', product);
    })

  } catch (error) {
    res.status(400).json(error);
  }

}

const informationProductCompany = (req, res) => {

  try {

    const productId = req.params._id

    Product.
    findById(productId).
    populate('company'). // only works if we pushed refs to children
    exec((err, data) => {
      if (err) return err;
      console.log(data);
      const companiesData = data

      res.status(200).json(companiesData)
      // res.status(200).json(reduceCompany)
    });

  } catch (error) {
    // console.log(error);
    res.status(400).json(error);
  }
}

const objModulesToExport = {
  createProduct,
  informationProductCompany
}

module.exports = objModulesToExport
