
const express = require('express');
const router = express.Router();

const Payment = require('../controller/payment')

// router post route payment, validating payment, create thruout payment
router.post('/payment', Payment.create);
router.get('/payment/:_id', Payment.informationPaymentCompanyProduct); 

module.exports = router
