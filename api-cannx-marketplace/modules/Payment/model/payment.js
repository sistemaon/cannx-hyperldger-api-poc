
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

// mongoose payment schema mongo
const PaymentSchema = new Schema({
	buyer: {type: String, required: true},
  paymentType: {type: String, required: true},
  total: {type: Number, required: true},
	sellers: [{ type: Schema.Types.ObjectId, ref: 'Company' }],
  products: [{ type: Schema.Types.ObjectId, ref: 'Product' }],

});

// mongoose payment model mongo
const Payment = mongoose.model('Payment', PaymentSchema);

module.exports = Payment;
