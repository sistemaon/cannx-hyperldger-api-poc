
const Payment = require('../model/payment');

const create = (req, res) => {

  try {
    // {
    //   "buyer": "kavallare@gmail.com",
    //   "paymentType": "Cryptocurrency",
    //   "sellers": ["5d77c870cfbf0964edd9c2b2", "5d76ab9de8c87e724424f596"],
    //   "products": ["5d77c8decfbf0964edd9c2b4", "5d76b77daa78ed7bf9d6aeec"]
    // }
    const objPayment = {
      buyer: req.body.buyer || 'Erni Souza',
      paymentType: req.body.paymentType || 'Cryptocurrency',
      sellers: req.body.sellers || ['id-s01', 'id-s02'],
      products: req.body.products || ['id-p01', 'id-p02']
    }

    const payment = Payment.create(objPayment);
    payment.then((newPayment) => {

        res.status(201).json(newPayment);

      })
      .catch(err => res.status(400).json(err))
    // console.log( 'payment ::; ', payment);

  } catch (error) {
    res.status(400).json(error);
  }

}

const informationPaymentCompanyProduct = (req, res) => {

  try {

    const paymentId = req.params._id

    Payment.
    findById(paymentId).
    populate('sellers'). // only works if we pushed refs to children
    populate('products'). // only works if we pushed refs to children
    exec( async (err, data) => {

      if (err) return err;
      console.log(data);

      const dataSellers = data.sellers
      const mapDataSellers = dataSellers.map(sellers => sellers.companyName);

      const dataProducts = data.products
      const mapDataProducts = dataProducts.map(products => {
        return {"Product Name": products.productName, "Product Price": products.price}
      });

      const mapDataProductsTotal = dataProducts.map(products => products.price);
      const reduceDataProductsTotal = mapDataProductsTotal.reduce((acc, curr) => acc + curr, 0);

      const objPaymentInformation = {
        sellers: mapDataSellers,
        products: mapDataProducts,
        total: reduceDataProductsTotal,
        paymentType: data.paymentType,
        buyerEmail: data.buyer
      }
      // const paymentInformation = data

      const dataMongoose = {
        "$class": "org.cannx.Payment",
        "paymentId": paymentId,
        "sellers": mapDataSellers,
        "products": mapDataProducts,
        "total": reduceDataProductsTotal,
        "paymentType": data.paymentType,
        "buyerEmail": data.buyer
        // "buyer": {},
        // "transactionId": "string",
        // "timestamp": "2019-09-11T21:52:07.799Z"
      }

      const URL_MONGOOSE_POST = 'http://localhost:3000/api/Payment';
      const result = await request.post({ uri: URL_MONGOOSE_POST, json: dataMongoose });

      res.status(200).json(objPaymentInformation)

    });

  } catch (error) {
    res.status(400).json(error);
  }
}


const objModulesToExport = {
  create,
  informationPaymentCompanyProduct,
}

module.exports = objModulesToExport
